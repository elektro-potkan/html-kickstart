Customized HTML Kickstart framework
===================================

HTML Kickstart by Joshua Gatcke http://www.99lime.com,
customized by Elektro-potkan <git@elektro-potkan.cz>.


Usage
-----
As there is currently no version tag for the original package in Packagist,
You need to explicitly depend on the same commit as this package in Your `composer.json`:
```json
{
	"require": {
		"99lime/html-kickstart": "dev-master#52f37f98a1525c7b4cc55ea9bd331dfb5a36bffb",
		"elektro-potkan/html-kickstart": "^2.0.0"
	}
}
```

After loading the original Kickstart files as needed, load also the customized CSS:
```html
<link rel="stylesheet" href="path-to-exposed-assets-from-vendor-directory/css/kickstart.custom.css" media="all" />
```


Authors
-------
- Customization files by Elektro-potkan <git@elektro-potkan.cz>.
- Original HTML Kickstart by Joshua Gatcke http://www.99lime.com.


Info
----
### Versioning
This project uses [Semantic Versioning 2.0.0 (semver.org)](https://semver.org).

### Branching
This project uses slightly modified Git-Flow Workflow and Branching Model:
- https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
- https://nvie.com/posts/a-successful-git-branching-model/


License
-------
This program is licensed under the MIT License.

See file [LICENSE](LICENSE).
